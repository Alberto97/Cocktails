package com.thecocktaildb.model.adapter

import org.junit.Test
import java.time.LocalDateTime

class DateTimeAdapterTests {
    @Test
    fun deserialize() {
        val string = "2015-08-18 14:42:59"

        val adapter = DateTimeAdapter()
        val date = adapter.fromJson(string)

        assert(2015 == date.year)
        assert(8 == date.monthValue)
        assert(18 == date.dayOfMonth)
        assert(14 == date.hour)
        assert(42 == date.minute)
        assert(59 == date.second)
    }

    @Test
    fun serialize() {
        val date = LocalDateTime.of(2022, 1, 1, 22, 30, 50)

        val adapter = DateTimeAdapter()
        val string = adapter.toJson(date)

        assert("2022-01-01 22:30:50" == string)
    }
}