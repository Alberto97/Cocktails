package com.thecocktaildb.data

import com.thecocktaildb.mock.Drinks
import com.thecocktaildb.model.CocktailFilter
import com.thecocktaildb.model.DrinkResponse
import com.thecocktaildb.model.Result
import com.thecocktaildb.network.CocktailApi
import com.thecocktaildb.tools.UriHelper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@OptIn(ExperimentalCoroutinesApi::class)
class CocktailRepositoryTest {
    @Mock
    private lateinit var api: CocktailApi

    private lateinit var repository: CocktailRepository

    @Before
    fun setUp() {
        val uriHelper: UriHelper = mock {
            on { buildIngredientThumbUrl(anyString()) }.thenReturn("")
        }
        api = mock()
        repository = CocktailRepositoryImpl(api, DrinkDetailsConverter(uriHelper), UnconfinedTestDispatcher())
    }

    @Test
    fun testGet() = runBlocking {
        val mockResponse = DrinkResponse(Drinks.alcoholic)
        whenever(api.filter(alcoholic = "Alcoholic")).thenReturn(mockResponse)

        val response = repository.getCocktails(CocktailFilter.Alcoholic, "Alcoholic")
        assert(response is Result.Success)

        val data = (response as Result.Success).data
        assertEquals(data[0].id, "15395")
        assertEquals(data[1].id, "15423")
        assertEquals(data[2].id, "14588")
    }

    @Test
    fun getDetails() = runBlocking {
        val mockResponse = DrinkResponse(Drinks.drinkDetails)
        whenever(api.getDetails("11007")).thenReturn(mockResponse)

        val response = repository.getDetails("11007")
        assert(response is Result.Success)

        val data = (response as Result.Success).data
        assertEquals(data.drink, "Margarita")
        assertEquals(data.drinkThumb, "https://www.thecocktaildb.com/images/media/drink/5noda61589575158.jpg")
        assertEquals(data.category,"Ordinary Drink")
        assertEquals(data.alcoholic, "Alcoholic")
        assertEquals(data.glass,"Cocktail glass")
        assertEquals(data.instructions, "Rub the rim of the glass with the lime slice to make the salt stick to it. Take care to moisten only the outer rim and sprinkle the salt on it. The salt should present to the lips of the imbiber and never mix into the cocktail. Shake the other ingredients with ice, then carefully pour into the glass.")
        assertEquals(data.ingredients[0].ingredient, "Tequila")
        assertEquals(data.ingredients[0].measure, "1 1/2 oz ")
        //assertEquals(data.ingredients[0].thumbUrl, "https://www.thecocktaildb.com/images/ingredients/Tequila-Small.png")
        assertEquals(data.ingredients[1].ingredient, "Triple sec")
        assertEquals(data.ingredients[1].measure, "1/2 oz ")
        //assertEquals(data.ingredients[1].thumbUrl, "https://www.thecocktaildb.com/images/ingredients/Triple%20sec-Small.png")
        assertEquals(data.ingredients[2].ingredient, "Lime juice")
        assertEquals(data.ingredients[2].measure, "1 oz ")
        //assertEquals(data.ingredients[2].thumbUrl, "https://www.thecocktaildb.com/images/ingredients/Lime%20juice-Small.png")
        assertEquals(data.ingredients[3].ingredient, "Salt")
        assertEquals(data.ingredients[3].measure, null)
        //assertEquals(data.ingredients[3].thumbUrl, "https://www.thecocktaildb.com/images/ingredients/Salt-Small.png")
    }
}