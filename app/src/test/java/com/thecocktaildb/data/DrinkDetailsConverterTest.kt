package com.thecocktaildb.data

import com.thecocktaildb.mock.Drinks
import com.thecocktaildb.tools.UriHelper
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.kotlin.mock

class DrinkDetailsConverterTest {

    private lateinit var converter: DrinkDetailsConverter

    @Before
    fun setup() {
        val uriHelper: UriHelper = mock {
            on { buildIngredientThumbUrl(anyString()) }.thenReturn("")
        }
        converter = DrinkDetailsConverter(uriHelper)
    }
    @Test
    fun testConvert() {
        val drinkDetails = Drinks.drinkDetails[0]

        val appDrinkDetails = converter.convert(drinkDetails)

        assertEquals(appDrinkDetails.drink, "Margarita")
        assertEquals(appDrinkDetails.drinkThumb, "https://www.thecocktaildb.com/images/media/drink/5noda61589575158.jpg")
        assertEquals(appDrinkDetails.category,"Ordinary Drink")
        assertEquals(appDrinkDetails.alcoholic, "Alcoholic")
        assertEquals(appDrinkDetails.glass,"Cocktail glass")
        assertEquals(appDrinkDetails.instructions, "Rub the rim of the glass with the lime slice to make the salt stick to it. Take care to moisten only the outer rim and sprinkle the salt on it. The salt should present to the lips of the imbiber and never mix into the cocktail. Shake the other ingredients with ice, then carefully pour into the glass.")
        assertEquals(appDrinkDetails.ingredients[0].ingredient, "Tequila")
        assertEquals(appDrinkDetails.ingredients[0].measure, "1 1/2 oz ")
        //assertEquals(appDrinkDetails.ingredients[0].thumbUrl, "https://www.thecocktaildb.com/images/ingredients/Tequila-Small.png")
        assertEquals(appDrinkDetails.ingredients[1].ingredient, "Triple sec")
        assertEquals(appDrinkDetails.ingredients[1].measure, "1/2 oz ")
        //assertEquals(appDrinkDetails.ingredients[1].thumbUrl, "https://www.thecocktaildb.com/images/ingredients/Triple%20sec-Small.png")
        assertEquals(appDrinkDetails.ingredients[2].ingredient, "Lime juice")
        assertEquals(appDrinkDetails.ingredients[2].measure, "1 oz ")
        //assertEquals(appDrinkDetails.ingredients[2].thumbUrl, "https://www.thecocktaildb.com/images/ingredients/Lime%20juice-Small.png")
        assertEquals(appDrinkDetails.ingredients[3].ingredient, "Salt")
        assertEquals(appDrinkDetails.ingredients[3].measure, null)
        //assertEquals(appDrinkDetails.ingredients[3].thumbUrl, "https://www.thecocktaildb.com/images/ingredients/Salt-Small.png")
    }
}