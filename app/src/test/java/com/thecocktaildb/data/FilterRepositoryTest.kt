package com.thecocktaildb.data

import com.thecocktaildb.mock.Filters
import com.thecocktaildb.model.DrinkResponse
import com.thecocktaildb.model.Result
import com.thecocktaildb.network.CocktailApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@OptIn(ExperimentalCoroutinesApi::class)
class FilterRepositoryTest {

    @Mock
    private lateinit var api: CocktailApi

    private lateinit var repository: FilterRepository

    @Before
    fun setup() {
        api = mock()
        repository = FilterRepositoryImpl(api, FilterConverter(), UnconfinedTestDispatcher())
    }

    @Test
    fun testCategoryFilters() = runBlocking {
        val mockResponse = DrinkResponse(Filters.categories)
        whenever(api.getCategoryFilters()).thenReturn(mockResponse)

        val result = repository.getCategoryFilters()
        assert(result is Result.Success)

        val data = (result as Result.Success).data
        assertEquals(data[0].label, "Ordinary Drink")
        assertEquals(data[0].value, "Ordinary_Drink")
        assertEquals(data[1].label, "Cocktail")
        assertEquals(data[1].value, "Cocktail")
    }

    @Test
    fun testIngredientsFilters() = runBlocking {
        val mockResponse = DrinkResponse(Filters.ingredients)
        whenever(api.getIngredientFilters()).thenReturn(mockResponse)

        val result = repository.getIngredientsFilters()
        assert(result is Result.Success)

        val data = (result as Result.Success).data
        assertEquals(data[0].label, "Light rum")
        assertEquals(data[0].value, "Light_rum")
        assertEquals(data[1].label, "Applejack")
        assertEquals(data[1].value, "Applejack")
    }

    @Test
    fun testGlassFilters() = runBlocking {
        val mockResponse = DrinkResponse(Filters.glasses)
        whenever(api.getGlassFilters()).thenReturn(mockResponse)

        val result = repository.getGlassFilters()
        assert(result is Result.Success)

        val data = (result as Result.Success).data
        assertEquals(data[0].label, "Highball glass")
        assertEquals(data[0].value, "Highball_glass")
    }

    @Test
    fun testAlcoholicFilters() = runBlocking {
        val mockResponse = DrinkResponse(Filters.alcoholic)
        whenever(api.getAlcoholicFilters()).thenReturn(mockResponse)

        val result = repository.getAlcoholicFilters()
        assert(result is Result.Success)

        val data = (result as Result.Success).data
        assertEquals(data[0].label, "Alcoholic")
        assertEquals(data[0].value, "Alcoholic")
        assertEquals(data[1].label, "Non alcoholic")
        assertEquals(data[1].value, "Non_alcoholic")
        assertEquals(data[2].label, "Optional alcohol")
        assertEquals(data[2].value, "Optional_alcohol")
    }
}