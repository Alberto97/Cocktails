package com.thecocktaildb.data

import org.junit.Assert.assertEquals
import org.junit.Test

class FilterConverterTest {
    @Test
    fun convert() {
        val input = "Coffee / Tea"
        val output = FilterConverter().convert(input)

        assertEquals(input, output.label)
        assertEquals("Coffee_/_Tea", output.value)
    }
}