package com.thecocktaildb.ui.details

import androidx.lifecycle.SavedStateHandle
import app.cash.turbine.test
import com.thecocktaildb.MainCoroutineRule
import com.thecocktaildb.data.CocktailRepository
import com.thecocktaildb.model.AppDrinkDetails
import com.thecocktaildb.model.AppIngredient
import com.thecocktaildb.model.Result
import com.thecocktaildb.model.UiState
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class DetailsViewModelTest {
    @Mock
    private lateinit var repository: CocktailRepository

    private val savedStateHandle = SavedStateHandle()

    private val drinkId = "11007"

    @get:Rule
    val coroutineRule = MainCoroutineRule()

    @Before
    fun setUp() {
        savedStateHandle.set("id", drinkId)
        repository = mock()
    }

    @Test
    fun testFailure() = runBlocking {
        val errorMessage = "Failure"
        whenever(repository.getDetails(drinkId)).thenReturn(Result.Error(errorMessage))
        val viewModel = DetailsViewModel(savedStateHandle, repository)

        viewModel.uiState.test {
            assertEquals(UiState.Failure, awaitItem())
        }

        viewModel.errorMessage.test {
            viewModel.loadDetails()
            assertEquals(errorMessage, awaitItem())
            cancelAndConsumeRemainingEvents()
        }

    }

    @Test
    fun testSuccess() = runBlocking {
        whenever(repository.getDetails(drinkId)).thenReturn(Result.Success(appDrinkDetails))
        val viewModel = DetailsViewModel(savedStateHandle, repository)

        viewModel.uiState.test {
            assertEquals(UiState.Success, awaitItem())
        }

        viewModel.title.test {
            assertEquals(appDrinkDetails.drink, awaitItem())
            cancelAndConsumeRemainingEvents()
        }
        viewModel.image.test {
            assertEquals(appDrinkDetails.drinkThumb, awaitItem())
            cancelAndConsumeRemainingEvents()
        }
        viewModel.category.test {
            assertEquals(appDrinkDetails.category, awaitItem())
            cancelAndConsumeRemainingEvents()
        }
        viewModel.alcoholic.test {
            assertEquals(appDrinkDetails.alcoholic, awaitItem())
            cancelAndConsumeRemainingEvents()
        }
        viewModel.glass.test {
            assertEquals(appDrinkDetails.glass, awaitItem())
            cancelAndConsumeRemainingEvents()
        }
        viewModel.instructions.test {
            assertEquals(appDrinkDetails.instructions, awaitItem())
            cancelAndConsumeRemainingEvents()
        }
        viewModel.ingredients.test {
            assertEquals(4, awaitItem().size)
            cancelAndConsumeRemainingEvents()
        }
    }

    private val appDrinkDetails = AppDrinkDetails(
        drink = "Margarita",
        drinkThumb = "https://www.thecocktaildb.com/images/media/drink/5noda61589575158.jpg",
        category = "Ordinary Drink",
        alcoholic = "Alcoholic",
        glass = "Cocktail glass",
        instructions = "Rub the rim of the glass with the lime slice to make the salt stick to it. Take care to moisten only the outer rim and sprinkle the salt on it. The salt should present to the lips of the imbiber and never mix into the cocktail. Shake the other ingredients with ice, then carefully pour into the glass.",
        ingredients = listOf(
            AppIngredient(
                "Tequila",
                "1 1/2 oz ",
                "https://www.thecocktaildb.com/images/ingredients/Tequila-Small.png"
            ),
            AppIngredient(
                "Triple sec",
                "1/2 oz ",
                "https://www.thecocktaildb.com/images/ingredients/Triple%20sec-Small.png"
            ),
            AppIngredient(
                "Lime juice",
                "1 oz ",
                "https://www.thecocktaildb.com/images/ingredients/Lime%20juice-Small.png"
            ),
            AppIngredient(
                "Salt",
                null,
                "https://www.thecocktaildb.com/images/ingredients/Salt-Small.png"
            )
        )
    )
}