package com.thecocktaildb.ui.cocktails

import app.cash.turbine.test
import com.thecocktaildb.MainCoroutineRule
import com.thecocktaildb.data.CocktailRepository
import com.thecocktaildb.data.FilterConverter
import com.thecocktaildb.data.FilterRepository
import com.thecocktaildb.mock.Drinks
import com.thecocktaildb.mock.Filters
import com.thecocktaildb.model.AppPrimaryFilter
import com.thecocktaildb.model.AppSecondaryFilter
import com.thecocktaildb.model.CocktailFilter
import com.thecocktaildb.model.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

@OptIn(ExperimentalCoroutinesApi::class)
class CocktailsViewModelTest {

    @Mock
    private lateinit var filterRepository: FilterRepository

    @Mock
    private lateinit var repository: CocktailRepository

    @get:Rule
    val coroutineRule = MainCoroutineRule()

    @Before
    fun setUp() {
        val converter = FilterConverter()
        val categories = Filters.categories.map { converter.convert(it.category) }
        val ingredients = Filters.ingredients.map { converter.convert(it.ingredient) }
        val glasses = Filters.glasses.map { converter.convert(it.glass) }
        val alcoholic = Filters.alcoholic.map { converter.convert(it.alcoholic) }

        filterRepository = mock {
            on { getPrimaryFilters() }.thenReturn(Filters.primary)
            onBlocking { getCategoryFilters() }.doReturn(Result.Success(categories))
            onBlocking { getIngredientsFilters() }.doReturn(Result.Success(ingredients))
            onBlocking { getGlassFilters() }.doReturn(Result.Success(glasses))
            onBlocking { getAlcoholicFilters() }.doReturn(Result.Success(alcoholic))
        }

        repository = mock {
            onBlocking { getCocktails(CocktailFilter.Alcoholic, "Alcoholic") }
                .doReturn(Result.Success(Drinks.alcoholic))
            onBlocking { getCocktails(CocktailFilter.Category, "Ordinary_Drink") }
                .doReturn(Result.Success(Drinks.categoryOrdinaryDrink))
            onBlocking { getCocktails(CocktailFilter.Category, "Cocktail") }
                .doReturn(Result.Success(Drinks.categoryCocktail))
        }
    }

    @Test
    fun testLaunch() = runBlocking {
        val viewModel = CocktailsViewModel(filterRepository, repository)

        viewModel.errorMessages.test {
            Assert.assertEquals(emptyList<String>(), awaitItem())
        }

        viewModel.primaryFilter.test {
            val filter = awaitItem()
            Assert.assertEquals("Alcoholic", filter.label)
            Assert.assertEquals(CocktailFilter.Alcoholic, filter.value)
            cancelAndConsumeRemainingEvents()
        }

        viewModel.secondaryFilterList.test {
            Assert.assertEquals(Filters.alcoholic.size, awaitItem().size)
        }

        viewModel.secondaryFilter.test {
            val filter = awaitItem()
            Assert.assertEquals("Alcoholic", filter.label)
            Assert.assertEquals("Alcoholic", filter.value)
            cancelAndConsumeRemainingEvents()
        }

        viewModel.list.test {
            Assert.assertEquals(Drinks.alcoholic.size, awaitItem().size)
            cancelAndConsumeRemainingEvents()
        }
    }

    @Test
    fun testPrimaryFilterChange() = runTest {
        val viewModel = CocktailsViewModel(filterRepository, repository)
        viewModel.primaryFilterChanged(AppPrimaryFilter("Category", CocktailFilter.Category))

        viewModel.primaryFilter.test {
            Assert.assertEquals(CocktailFilter.Category, awaitItem().value)
            cancelAndConsumeRemainingEvents()
        }

        viewModel.secondaryFilterList.test {
            Assert.assertEquals(Filters.categories.size, awaitItem().size)
            cancelAndConsumeRemainingEvents()
        }

        viewModel.secondaryFilter.test {
            Assert.assertEquals("Ordinary Drink", awaitItem().label)
            cancelAndConsumeRemainingEvents()
        }

        viewModel.list.test {
            Assert.assertEquals(Drinks.categoryOrdinaryDrink.size, awaitItem().size)
            cancelAndConsumeRemainingEvents()
        }
    }

    @Test
    fun testSecondaryFilterChange() = runBlocking {
        val viewModel = CocktailsViewModel(filterRepository, repository)
        viewModel.primaryFilterChanged(AppPrimaryFilter("Category", CocktailFilter.Category))

        viewModel.secondaryFilter.test {
            viewModel.secondaryFilterChanged(AppSecondaryFilter("Cocktail", "Cocktail"))
            awaitItem()
            val item = awaitItem()
            Assert.assertEquals("Cocktail", item.label)
            cancelAndConsumeRemainingEvents()
        }


        viewModel.list.test {
            Assert.assertEquals(Drinks.categoryCocktail.size, awaitItem().size)
            cancelAndConsumeRemainingEvents()
        }

    }
}