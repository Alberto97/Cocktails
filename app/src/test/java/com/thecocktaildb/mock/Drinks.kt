package com.thecocktaildb.mock

import com.thecocktaildb.model.Drink
import com.thecocktaildb.model.DrinkDetails
import java.time.LocalDateTime

object Drinks {
    val alcoholic = listOf(
        Drink(
            "15395",
            "1-900-FUK-MEUP",
            "https://www.thecocktaildb.com/images/media/drink/uxywyw1468877224.jpg"
        ),
        Drink(
            "15423",
            "110 in the shade",
            "https://www.thecocktaildb.com/images/media/drink/xxyywq1454511117.jpg"
        ),
        Drink(
            "14588",
            "151 Florida Bushwacker",
            "https://www.thecocktaildb.com/images/media/drink/rvwrvv1468877323.jpg"
        )
    )

    val categoryOrdinaryDrink = listOf(
        Drink(
            "15300",
            "3-Mile Long Island Iced Tea",
            "https://www.thecocktaildb.com/images/media/drink/rrtssw1472668972.jpg",
        ),
        Drink(
            "13581",
            "410 Gone",
            "https://www.thecocktaildb.com/images/media/drink/xtuyqv1472669026.jpg",
        ),
        Drink(
            "14598",
            "50/50",
            "https://www.thecocktaildb.com/images/media/drink/wwpyvr1461919316.jpg",
        ),
        Drink(
            "17105",
            "501 Blue",
            "https://www.thecocktaildb.com/images/media/drink/ywxwqs1461867097.jpg",
        )
    )

    val categoryCocktail = listOf(
        Drink(
            drink ="155 Belmont",
            drinkThumb ="https://www.thecocktaildb.com/images/media/drink/yqvvqs1475667388.jpg",
            id ="15346"
        ),
        Drink(
            drink ="57 Chevy with a White License Plate",
            drinkThumb ="https://www.thecocktaildb.com/images/media/drink/qyyvtu1468878544.jpg",
            id ="14029"
        ),
        Drink(
            drink ="747 Drink",
            drinkThumb ="https://www.thecocktaildb.com/images/media/drink/i9suxb1582474926.jpg",
            id ="178318"
        ),
        Drink(
            drink ="9 1/2 Weeks",
            drinkThumb ="https://www.thecocktaildb.com/images/media/drink/xvwusr1472669302.jpg",
            id ="16108"
        ),
        Drink(
            drink ="A Gilligan's Island",
            drinkThumb ="https://www.thecocktaildb.com/images/media/drink/wysqut1461867176.jpg",
            id ="16943"
        ),
        Drink(
            drink ="A True Amaretto Sour",
            drinkThumb ="https://www.thecocktaildb.com/images/media/drink/rptuxy1472669372.jpg",
            id ="17005"
        ),
        Drink(
            drink ="A.D.M. (After Dinner Mint)",
            drinkThumb ="https://www.thecocktaildb.com/images/media/drink/ruxuvp1472669600.jpg",
            id ="14560"
        ),
        Drink(
            drink ="A1",
            drinkThumb ="https://www.thecocktaildb.com/images/media/drink/2x8thr1504816928.jpg",
            id ="17222"
        )
    )

    val drinkDetails = listOf(
        DrinkDetails(
            id = "11007",
            drink = "Margarita",
            drinkAlternate = null,
            tags = "IBA,ContemporaryClassic",
            video = null,
            category = "Ordinary Drink",
            IBA = "Contemporary Classics",
            alcoholic = "Alcoholic",
            glass = "Cocktail glass",
            instructions = "Rub the rim of the glass with the lime slice to make the salt stick to it. Take care to moisten only the outer rim and sprinkle the salt on it. The salt should present to the lips of the imbiber and never mix into the cocktail. Shake the other ingredients with ice, then carefully pour into the glass.",
            instructionsES = null,
            instructionsDE = "Reiben Sie den Rand des Glases mit der Limettenscheibe, damit das Salz daran haftet. Achten Sie darauf, dass nur der äußere Rand angefeuchtet wird und streuen Sie das Salz darauf. Das Salz sollte sich auf den Lippen des Genießers befinden und niemals in den Cocktail einmischen. Die anderen Zutaten mit Eis schütteln und vorsichtig in das Glas geben.",
            instructionsFR = null,
            instructionsIT = "Strofina il bordo del bicchiere con la fetta di lime per far aderire il sale.\r\nAvere cura di inumidire solo il bordo esterno e cospargere di sale.\r\nIl sale dovrebbe presentarsi alle labbra del bevitore e non mescolarsi mai al cocktail.\r\nShakerare gli altri ingredienti con ghiaccio, quindi versarli delicatamente nel bicchiere.",
            drinkThumb = "https://www.thecocktaildb.com/images/media/drink/5noda61589575158.jpg",
            ingredient1 = "Tequila",
            ingredient2 = "Triple sec",
            ingredient3 = "Lime juice",
            ingredient4 = "Salt",
            ingredient5 = null,
            ingredient6 = null,
            ingredient7 = null,
            ingredient8 = null,
            ingredient9 = null,
            ingredient10 = null,
            ingredient11 = null,
            ingredient12 = null,
            ingredient13 = null,
            ingredient14 = null,
            ingredient15 = null,
            measure1 = "1 1/2 oz ",
            measure2 = "1/2 oz ",
            measure3 = "1 oz ",
            measure4 = null,
            measure5 = null,
            measure6 = null,
            measure7 = null,
            measure8 = null,
            measure9 = null,
            measure10 = null,
            measure11 = null,
            measure12 = null,
            measure13 = null,
            measure14 = null,
            measure15 = null,
            imageSource = "https://commons.wikimedia.org/wiki/File:Klassiche_Margarita.jpg",
            imageAttribution = "Cocktailmarler",
            creativeCommonsConfirmed = "Yes",
            dateModified = LocalDateTime.of(2015, 8, 18, 14, 42, 59)
        )
    )
}