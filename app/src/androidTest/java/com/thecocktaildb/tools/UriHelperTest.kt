package com.thecocktaildb.tools

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class UriHelperTest {

    @Test
    fun testImageUrl() {
        val id = "Light rum"
        val imageUrl = UriHelperImpl().buildIngredientThumbUrl(id)
        assertEquals(
            "https://www.thecocktaildb.com/images/ingredients/Light%20rum-Small.png",
            imageUrl
        )
    }
}