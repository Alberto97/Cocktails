package com.thecocktaildb.data

import com.thecocktaildb.model.DrinkDetails
import com.thecocktaildb.model.AppDrinkDetails
import com.thecocktaildb.model.AppIngredient
import com.thecocktaildb.tools.UriHelper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DrinkDetailsConverter @Inject constructor(private val uriHelper: UriHelper) {
    fun convert(drink: DrinkDetails): AppDrinkDetails {
        return AppDrinkDetails(
            drink = drink.drink,
            drinkThumb = drink.drinkThumb,
            category = drink.category,
            alcoholic = drink.alcoholic,
            glass = drink.glass,
            instructions = drink.instructions,
            ingredients = mapIngredients(drink)
        )
    }

    private fun mapIngredients(data: DrinkDetails): List<AppIngredient> {
        val list = (0..14).mapNotNull { i ->
            val ingredient = data.ingredients[i]
            val measure = data.measures[i]
            if (!ingredient.isNullOrEmpty())
                mapIngredient(ingredient, measure)
            else
                null
        }

        return list
    }

    private fun mapIngredient(ingredient: String, measure: String?): AppIngredient {
        val url = uriHelper.buildIngredientThumbUrl(ingredient)
        return AppIngredient(ingredient, measure, url)
    }
}