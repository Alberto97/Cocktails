package com.thecocktaildb.data

import com.thecocktaildb.model.AppSecondaryFilter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FilterConverter @Inject constructor() {
    fun convert(value: String): AppSecondaryFilter {
        return AppSecondaryFilter(value, convertValue(value))
    }

    private fun convertValue(value: String): String {
        return value.replace(" ", "_")
    }
}