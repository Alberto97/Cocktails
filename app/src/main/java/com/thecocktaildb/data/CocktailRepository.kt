package com.thecocktaildb.data

import com.thecocktaildb.model.AppDrinkDetails
import com.thecocktaildb.model.CocktailFilter
import com.thecocktaildb.model.Drink
import com.thecocktaildb.model.Result
import com.thecocktaildb.network.CocktailApi
import com.thecocktaildb.tools.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

interface CocktailRepository {
    suspend fun getCocktails(type: CocktailFilter, filter: String): Result<List<Drink>>
    suspend fun getDetails(id: String): Result<AppDrinkDetails>
}

@Singleton
class CocktailRepositoryImpl @Inject constructor(
    private val api: CocktailApi,
    private val drinkDetailsConverter: DrinkDetailsConverter,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : CocktailRepository {
    override suspend fun getCocktails(type: CocktailFilter, filter: String): Result<List<Drink>> = withContext(ioDispatcher) {
        return@withContext try {
            val resp = when (type) {
                CocktailFilter.Category -> api.filter(category = filter)
                CocktailFilter.Glass -> api.filter(glass = filter)
                CocktailFilter.Ingredient -> api.filter(ingredient = filter)
                CocktailFilter.Alcoholic -> api.filter(alcoholic = filter)
            }
            Result.Success(resp.drinks)
        } catch (e: Exception) {
            val msg = when (e) {
                is IOException -> "Check your connectivity"
                else -> "Failed to fetch cocktails"
            }
            Result.Error(msg)
        }
    }

    override suspend fun getDetails(id: String): Result<AppDrinkDetails> = withContext(ioDispatcher) {
        return@withContext try {
            val details = api.getDetails(id)
            val drink = drinkDetailsConverter.convert(details.drinks[0])
            Result.Success(drink)
        } catch (e: Exception) {
            val msg = when (e) {
                is IOException -> "Check your connectivity"
                else -> "Failed to fetch cocktail"
            }
            Result.Error(msg)
        }
    }
}