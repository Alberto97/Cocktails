package com.thecocktaildb.data

import com.thecocktaildb.model.AppPrimaryFilter
import com.thecocktaildb.model.AppSecondaryFilter
import com.thecocktaildb.model.CocktailFilter
import com.thecocktaildb.model.Result
import com.thecocktaildb.network.CocktailApi
import com.thecocktaildb.tools.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

interface FilterRepository {
    fun getPrimaryFilters(): List<AppPrimaryFilter>
    suspend fun getCategoryFilters(): Result<List<AppSecondaryFilter>>
    suspend fun getIngredientsFilters(): Result<List<AppSecondaryFilter>>
    suspend fun getGlassFilters(): Result<List<AppSecondaryFilter>>
    suspend fun getAlcoholicFilters(): Result<List<AppSecondaryFilter>>
}

@Singleton
class FilterRepositoryImpl @Inject constructor(
    private val api: CocktailApi,
    private val filterConverter: FilterConverter,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : FilterRepository {

    override fun getPrimaryFilters(): List<AppPrimaryFilter> {
        return listOf(
            AppPrimaryFilter("Alcoholic", CocktailFilter.Alcoholic),
            AppPrimaryFilter("Category", CocktailFilter.Category),
            AppPrimaryFilter("Glass", CocktailFilter.Glass),
            AppPrimaryFilter("Ingredient", CocktailFilter.Ingredient),
        )
    }

    override suspend fun getCategoryFilters(): Result<List<AppSecondaryFilter>> = withContext(ioDispatcher) {
        return@withContext try {
            val resp = api.getCategoryFilters()
            val list = resp.drinks.map { drink -> drink.category }
            val pairList = list.map { category -> filterConverter.convert(category) }
            Result.Success(pairList)
        } catch (e: Exception) {
            val msg = when (e) {
                is IOException -> "Check your connectivity"
                else -> "Failed to fetch category filters"
            }
            Result.Error(msg)
        }
    }

    override suspend fun getIngredientsFilters(): Result<List<AppSecondaryFilter>> = withContext(ioDispatcher) {
        return@withContext try {
            val resp = api.getIngredientFilters()
            val list = resp.drinks.map { drink -> drink.ingredient }
            val pairList = list.map { ingredient -> filterConverter.convert(ingredient) }
            Result.Success(pairList)
        } catch (e: Exception) {
            val msg = when (e) {
                is IOException -> "Check your connectivity"
                else -> "Failed to fetch ingredient filters"
            }
            Result.Error(msg)
        }
    }

    override suspend fun getGlassFilters(): Result<List<AppSecondaryFilter>> = withContext(ioDispatcher) {
        return@withContext try {
            val resp = api.getGlassFilters()
            val list = resp.drinks.map { drink -> drink.glass }
            val pairList = list.map { glass -> filterConverter.convert(glass) }
            Result.Success(pairList)
        } catch (e: Exception) {
            val msg = when (e) {
                is IOException -> "Check your connectivity"
                else -> "Failed to fetch  glass filters"
            }
            Result.Error(msg)
        }
    }

    override suspend fun getAlcoholicFilters(): Result<List<AppSecondaryFilter>> = withContext(ioDispatcher) {
        return@withContext try {
            val resp = api.getAlcoholicFilters()
            val list = resp.drinks.map { drink -> drink.alcoholic }
            val pairList = list.map { alcoholic -> filterConverter.convert(alcoholic) }
            Result.Success(pairList)
        } catch (e: Exception) {
            val msg = when (e) {
                is IOException -> "Check your connectivity"
                else -> "Failed to fetch alcoholic filters"
            }
            Result.Error(msg)
        }
    }
}