package com.thecocktaildb.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

enum class CocktailFilter {
    Category,
    Glass,
    Ingredient,
    Alcoholic
}

@JsonClass(generateAdapter = true)
class CategoryFilter(@Json(name = "strCategory") val category: String)

@JsonClass(generateAdapter = true)
class GlassFilter(@Json(name = "strGlass") val glass: String)

@JsonClass(generateAdapter = true)
class IngredientFilter(@Json(name = "strIngredient1") val ingredient: String)

@JsonClass(generateAdapter = true)
class AlcoholicFilter(@Json(name = "strAlcoholic") val alcoholic: String)
