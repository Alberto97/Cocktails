package com.thecocktaildb.model

data class AppPrimaryFilter(val label: String, val value: CocktailFilter)

data class AppSecondaryFilter(val label: String, val value: String)

class AppIngredient(val ingredient: String, val measure: String?, val thumbUrl: String)

data class AppDrinkDetails(
    val drink: String,
    val drinkThumb: String,
    val category: String,
    val alcoholic: String,
    val glass: String,
    val instructions: String,
    val ingredients: List<AppIngredient>
)