package com.thecocktaildb.model.adapter

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonQualifier
import com.squareup.moshi.ToJson
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class DateTime

class DateTimeAdapter {
    companion object {
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    }

    @ToJson
    fun toJson(@DateTime value: LocalDateTime?): String {
        return formatter.format(value)
    }

    @FromJson
    @DateTime
    fun fromJson(value: String?): LocalDateTime {
        return LocalDateTime.parse(value, formatter)
    }
}