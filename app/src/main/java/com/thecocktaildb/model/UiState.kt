package com.thecocktaildb.model

enum class UiState {
    Loading,
    Success,
    Failure
}