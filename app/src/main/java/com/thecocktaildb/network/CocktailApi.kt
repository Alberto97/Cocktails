package com.thecocktaildb.network

import com.thecocktaildb.model.*
import retrofit2.http.GET
import retrofit2.http.Query

interface CocktailApi {
    @GET("filter.php")
    suspend fun filter(
        @Query("c") category: String? = null,
        @Query("g") glass: String? = null,
        @Query("i") ingredient: String? = null,
        @Query("a") alcoholic: String? = null
    ): DrinkResponse<Drink>

    @GET("lookup.php")
    suspend fun getDetails(@Query("i") id: String): DrinkResponse<DrinkDetails>

    @GET("list.php?c=list")
    suspend fun getCategoryFilters(): DrinkResponse<CategoryFilter>

    @GET("list.php?i=list")
    suspend fun getIngredientFilters(): DrinkResponse<IngredientFilter>

    @GET("list.php?g=list")
    suspend fun getGlassFilters(): DrinkResponse<GlassFilter>

    @GET("list.php?a=list")
    suspend fun getAlcoholicFilters(): DrinkResponse<AlcoholicFilter>
}