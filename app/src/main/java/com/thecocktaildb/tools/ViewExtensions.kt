package com.thecocktaildb.tools

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

object ViewExtensions {
    fun ViewGroup.inflater(): LayoutInflater {
        return LayoutInflater.from(this.context)
    }

    fun View.setVisible(show: Boolean) {
        val state = if (show) View.VISIBLE else View.GONE
        visibility = state
    }
}