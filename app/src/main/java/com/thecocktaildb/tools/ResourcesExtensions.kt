package com.thecocktaildb.tools

import android.content.res.Resources
import android.util.TypedValue

object ResourcesExtensions {
    fun Resources.dpToPixel(value: Float): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            value,
            displayMetrics
        )
    }
}