package com.thecocktaildb.tools

import android.net.Uri
import com.thecocktaildb.BuildConfig
import javax.inject.Inject
import javax.inject.Singleton

interface UriHelper {
    fun buildIngredientThumbUrl(value: String): String
}

@Singleton
class UriHelperImpl @Inject constructor() : UriHelper {
    override fun buildIngredientThumbUrl(value: String): String {
        val builtUri: Uri = Uri.parse(BuildConfig.INGREDIENTS_THUMB_URL)
            .buildUpon()
            .appendPath("${value}-Small.png")
            .build()
        return builtUri.toString()
    }

}