package com.thecocktaildb.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import coil.load
import coil.size.Scale
import coil.transform.RoundedCornersTransformation
import com.google.android.material.snackbar.Snackbar
import com.thecocktaildb.R
import com.thecocktaildb.databinding.DetailsFragmentBinding
import com.thecocktaildb.model.UiState
import com.thecocktaildb.tools.ResourcesExtensions.dpToPixel
import com.thecocktaildb.tools.ViewExtensions.setVisible
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class DetailsFragment : Fragment() {

    private lateinit var binding: DetailsFragmentBinding
    private val viewModel: DetailsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DetailsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Toolbar
        binding.toolbar.setupWithNavController(findNavController())

        // Ingredients
        val adapter = DetailsAdapter()
        binding.ingredients.adapter = adapter

        binding.tryAgainButton.setOnClickListener {
            viewModel.loadDetails()
        }

        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.errorMessage.collect { message ->
                        val snack = Snackbar.make(binding.coordinatorLayout, message, Snackbar.LENGTH_INDEFINITE)
                        snack.setAction(R.string.action_try_again) {
                            snack.dismiss()
                            viewModel.loadDetails()
                        }
                        snack.show()
                    }
                }
                launch {
                    viewModel.uiState.collect { state ->
                        binding.content.setVisible(state == UiState.Success)
                        binding.loadingLayout.setVisible(state == UiState.Loading)
                        binding.failedLayout.setVisible(state == UiState.Failure)
                    }
                }

                launch {
                    viewModel.title.collect { title ->
                        binding.toolbar.title = title
                        binding.title.text = title
                    }
                }
                launch {
                    viewModel.image.collect { imageUrl ->
                        val thumbSize = resources.dpToPixel(128f).toInt()
                        binding.image.load(imageUrl) {
                            size(thumbSize, thumbSize)
                            transformations(RoundedCornersTransformation(20f))
                            scale(Scale.FIT)
                        }
                    }
                }
                launch {
                    viewModel.category.collect { category ->
                        binding.category.text = category
                    }
                }
                launch {
                    viewModel.alcoholic.collect { alcoholic ->
                        binding.alcoholic.text = alcoholic
                    }
                }
                launch {
                    viewModel.glass.collect { glass ->
                        binding.glass.text = glass
                    }
                }
                launch {
                    viewModel.instructions.collect { instr ->
                        binding.instructions.text = instr
                    }
                }
                launch {
                    viewModel.ingredients.collect { list ->
                        adapter.submitList(list)
                    }
                }
            }
        }


    }
}