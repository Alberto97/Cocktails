package com.thecocktaildb.ui.details

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.size.Scale
import com.thecocktaildb.R
import com.thecocktaildb.databinding.ListItemIngredientsBinding
import com.thecocktaildb.model.AppIngredient
import com.thecocktaildb.tools.ResourcesExtensions.dpToPixel
import com.thecocktaildb.tools.ViewExtensions.inflater

class DetailsAdapter : ListAdapter<AppIngredient, DetailsAdapter.ViewHolder>(IngredientDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ListItemIngredientsBinding.inflate(parent.inflater())
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))


    inner class ViewHolder(private val binding: ListItemIngredientsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: AppIngredient) {
            val resources = binding.root.resources
            val thumbSize = resources.dpToPixel(48f).toInt()

            binding.ingredient.text = if (!item.measure.isNullOrEmpty()) {
                resources.getString(R.string.cocktail_ingredient_measure, item.ingredient, item.measure)
            } else {
                item.ingredient
            }

            binding.image.load(item.thumbUrl) {
                size(thumbSize, thumbSize)
                scale(Scale.FIT)
            }
        }
    }

}

private class IngredientDiff : DiffUtil.ItemCallback<AppIngredient>() {
    override fun areItemsTheSame(
        oldItem: AppIngredient,
        newItem: AppIngredient
    ): Boolean {
        return oldItem.ingredient == newItem.ingredient
    }

    override fun areContentsTheSame(
        oldItem: AppIngredient,
        newItem: AppIngredient
    ): Boolean {
        return oldItem.ingredient == newItem.ingredient && oldItem.measure == newItem.measure
    }
}
