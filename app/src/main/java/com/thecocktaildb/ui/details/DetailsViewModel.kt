package com.thecocktaildb.ui.details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.thecocktaildb.data.CocktailRepository
import com.thecocktaildb.model.AppDrinkDetails
import com.thecocktaildb.model.AppIngredient
import com.thecocktaildb.model.Result
import com.thecocktaildb.model.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val repository: CocktailRepository
) : ViewModel() {

    private val args = DetailsFragmentArgs.fromSavedStateHandle(savedStateHandle)

    private val _errorMessage = MutableSharedFlow<String>()
    val errorMessage = _errorMessage.asSharedFlow()

    private val _uiState = MutableStateFlow(UiState.Loading)
    val uiState = _uiState.asStateFlow()

    private val _title = MutableStateFlow("")
    val title = _title.asStateFlow()

    private val _image = MutableStateFlow("")
    val image = _image.asStateFlow()

    private val _category = MutableStateFlow("")
    val category = _category.asStateFlow()

    private val _alcoholic = MutableStateFlow("")
    val alcoholic = _alcoholic.asStateFlow()

    private val _glass = MutableStateFlow("")
    val glass = _glass.asStateFlow()

    private val _instructions = MutableStateFlow("")
    val instructions = _instructions.asStateFlow()

    private val _ingredients = MutableStateFlow(listOf<AppIngredient>())
    val ingredients = _ingredients.asStateFlow()

    init {
        loadDetails()
    }

    fun loadDetails() {
        viewModelScope.launch {
            _uiState.value = UiState.Loading
            when (val result = repository.getDetails(args.id)) {
                is Result.Success -> {
                    handleSuccess(result.data)
                    _uiState.value = UiState.Success
                }
                is Result.Error -> {
                    _errorMessage.emit(result.message)
                    _uiState.value = UiState.Failure
                }
            }
        }
    }

    private fun handleSuccess(data: AppDrinkDetails) {
        _title.value = data.drink
        _image.value = data.drinkThumb
        _category.value = data.category
        _alcoholic.value = data.alcoholic
        _glass.value = data.glass
        _instructions.value = data.instructions
        _ingredients.value = data.ingredients
    }

}