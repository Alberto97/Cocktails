package com.thecocktaildb.ui.cocktails

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.rounded.FilterList
import androidx.compose.material.icons.rounded.LocalBar
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.thecocktaildb.R
import com.thecocktaildb.model.*
import com.thecocktaildb.ui.theme.CocktailTheme

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun CocktailsScreen(viewModel: CocktailsViewModel, navigateToDetails: (value: Drink) -> Unit) {
    val errorMessages by viewModel.errorMessages.collectAsState()
    val uiState by viewModel.uiState.collectAsState()
    val list by viewModel.list.collectAsState()
    val primaryFilter by viewModel.primaryFilter.collectAsState()
    val primaryFilterList = viewModel.primaryFilterList
    val secondaryFilter by viewModel.secondaryFilter.collectAsState()
    val secondaryFilterList by viewModel.secondaryFilterList.collectAsState(listOf())


    CocktailsScreen(
        uiState = uiState,
        list = list,
        onItemClick = navigateToDetails,
        errorMessages = errorMessages,
        onDismissErrorMessage = { viewModel.dismissErrorMessage() },
        primaryFilter = primaryFilter,
        primaryFilterList = primaryFilterList,
        primaryFilterChanged = { filter -> viewModel.primaryFilterChanged(filter) },
        secondaryFilter = secondaryFilter,
        secondaryFilterList = secondaryFilterList,
        secondaryFilterChanged = { filter -> viewModel.secondaryFilterChanged(filter) }
    )
}

@ExperimentalMaterialApi
@Composable
private fun CocktailsScreen(
    uiState: UiState,
    list: List<Drink>,
    onItemClick: (value: Drink) -> Unit,
    errorMessages: List<String>,
    onDismissErrorMessage: () -> Unit,
    primaryFilter: AppPrimaryFilter,
    primaryFilterList: List<AppPrimaryFilter>,
    primaryFilterChanged: (value: AppPrimaryFilter) -> Unit,
    secondaryFilter: AppSecondaryFilter,
    secondaryFilterList: List<AppSecondaryFilter>,
    secondaryFilterChanged: (value: AppSecondaryFilter) -> Unit,
) {
    val scaffoldState = rememberScaffoldState()

    if (errorMessages.isNotEmpty()) {
        LaunchedEffect(errorMessages, scaffoldState) {
            scaffoldState.snackbarHostState.showSnackbar(errorMessages[0])
            onDismissErrorMessage()
        }
    }

    AppBottomSheet(sheetContent = { close ->
        CocktailBottomSheetFilters(
            primaryFilter,
            primaryFilterList,
            primaryFilterChanged,
            secondaryFilter,
            secondaryFilterList,
            secondaryFilterChanged,
            close,
        )
    }) { openSheet ->
        Scaffold(
            scaffoldState = scaffoldState,
            topBar = { CocktailToolbar(openSheet) }
        ) {
            when (uiState) {
                UiState.Loading -> Loading()
                UiState.Success -> Success(list, onItemClick)
                UiState.Failure -> Error()
            }
        }

    }
}

@ExperimentalMaterialApi
@Composable
private fun CocktailBottomSheetFilters(
    primaryFilter: AppPrimaryFilter,
    primaryFilterList: List<AppPrimaryFilter>,
    primaryFilterChanged: (value: AppPrimaryFilter) -> Unit,
    secondaryFilter: AppSecondaryFilter,
    secondaryFilterList: List<AppSecondaryFilter>,
    secondaryFilterChanged: (value: AppSecondaryFilter) -> Unit,
    closeBottomSheet: () -> Unit
) {
    Column(modifier = Modifier.padding(16.dp)) {
        Text(stringResource(R.string.filter_type), style = MaterialTheme.typography.h6)
        PrimaryFilter(
            Modifier.padding(top = 12.dp, bottom = 12.dp),
            primaryFilter, primaryFilterList, primaryFilterChanged
        )

        Text(stringResource(R.string.filter_by), style = MaterialTheme.typography.h6)
        SecondaryFilter(
            modifier = Modifier.padding(top = 12.dp, bottom = 12.dp),
            secondaryFilter, secondaryFilterList, secondaryFilterChanged
        )

        Button(
            onClick = closeBottomSheet,
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Text(stringResource(R.string.action_close))
        }
    }
}

@ExperimentalMaterialApi
@Composable
private fun SelectedChipIcon() {
    Icon(
        imageVector = Icons.Filled.Done,
        contentDescription = "",
        modifier = Modifier.size(ChipDefaults.SelectedIconSize)
    )
}

@Composable
private fun CocktailToolbar(openSheet: () -> Unit) {
    TopAppBar(
        title = { Text(stringResource(R.string.app_name)) },
        actions = {
            IconButton({ openSheet() }) {
                Icon(
                    Icons.Rounded.FilterList,
                    contentDescription = null
                )
            }
        }
    )
}

@ExperimentalMaterialApi
@Composable
private fun PrimaryFilter(
    modifier: Modifier = Modifier,
    primaryFilter: AppPrimaryFilter,
    primaryFilterList: List<AppPrimaryFilter>,
    primaryFilterChanged: (value: AppPrimaryFilter) -> Unit,
) {
    Row(
        modifier = modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        primaryFilterList.map { filter ->
            FilterChip(
                selected = filter == primaryFilter,
                onClick = { primaryFilterChanged(filter) },
                selectedIcon = { SelectedChipIcon() }
            ) {
                Text(filter.label)
            }
        }
    }
}

@ExperimentalMaterialApi
@Composable
private fun SecondaryFilter(
    modifier: Modifier = Modifier,
    secondaryFilter: AppSecondaryFilter,
    secondaryFilterList: List<AppSecondaryFilter>,
    secondaryFilterChanged: (value: AppSecondaryFilter) -> Unit,
) {
    AppExposedDropdownMenu(secondaryFilter.label, modifier.fillMaxWidth()) { close ->
        secondaryFilterList.forEach { item ->
            DropdownMenuItem(
                onClick = {
                    secondaryFilterChanged(item)
                    close()
                }
            ) {
                ListItem(
                    text = { Text(item.label) }
                )
            }
        }
    }
}

@Composable
private fun Loading() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator()
    }
}

@Composable
private fun Error() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.disabled) {
            Icon(Icons.Rounded.LocalBar, "", modifier = Modifier.size(128.dp))
            Text(
                stringResource(R.string.cocktails_error),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.body1
            )
        }
    }
}

@ExperimentalMaterialApi
@Composable
private fun Success(
    list: List<Drink>,
    onItemClick: (value: Drink) -> Unit,
) {
    LazyColumn {
        items(list) { item ->
            ListItem(
                icon = {
                    Image(
                        painter = rememberAsyncImagePainter(item.drinkThumb),
                        contentDescription = null,
                        modifier = Modifier
                            .size(48.dp)
                            .clip(RoundedCornerShape(8.dp))
                    )
                },
                text = { Text(item.drink) },
                modifier = Modifier.clickable { onItemClick(item) }
            )
        }

    }
}

@ExperimentalMaterialApi
@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    val primaryFilter = listOf(
        AppPrimaryFilter("Alcoholic", CocktailFilter.Alcoholic),
        AppPrimaryFilter("Category", CocktailFilter.Category),
        AppPrimaryFilter("Glass", CocktailFilter.Glass),
        AppPrimaryFilter("Ingredient", CocktailFilter.Ingredient),
    )
    val secondaryFilter = listOf(
        AppSecondaryFilter("Alcoholic", "Alcoholic"),
        AppSecondaryFilter("Non alcoholic", "Non_alcoholic"),
        AppSecondaryFilter("Optional alcohol", "Optional_alcohol")
    )
    CocktailTheme {
        CocktailsScreen(
            uiState = UiState.Failure,
            list = listOf(),
            onItemClick = {},
            errorMessages = listOf(),
            onDismissErrorMessage = {},
            primaryFilter = primaryFilter.first(),
            primaryFilterList = primaryFilter,
            primaryFilterChanged = {},
            secondaryFilter = secondaryFilter.first(),
            secondaryFilterList = listOf(),
            secondaryFilterChanged = {}
        )
    }
}

@ExperimentalMaterialApi
@Preview(showBackground = true)
@Composable
private fun FiltersPreview() {
    val primaryFilter = listOf(
        AppPrimaryFilter("Alcoholic", CocktailFilter.Alcoholic),
        AppPrimaryFilter("Category", CocktailFilter.Category),
        AppPrimaryFilter("Glass", CocktailFilter.Glass),
        AppPrimaryFilter("Ingredient", CocktailFilter.Ingredient),
    )
    val secondaryFilter = listOf(
        AppSecondaryFilter("Alcoholic", "Alcoholic"),
        AppSecondaryFilter("Non alcoholic", "Non_alcoholic"),
        AppSecondaryFilter("Optional alcohol", "Optional_alcohol")
    )
    CocktailTheme {
        CocktailBottomSheetFilters(
            primaryFilter = primaryFilter.first(),
            primaryFilterList = primaryFilter,
            primaryFilterChanged = {},
            secondaryFilter = secondaryFilter.first(),
            secondaryFilterList = secondaryFilter,
            secondaryFilterChanged = {},
            closeBottomSheet = {}
        )
    }
}