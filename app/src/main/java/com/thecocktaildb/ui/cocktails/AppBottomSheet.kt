package com.thecocktaildb.ui.cocktails

import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetLayout
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AppBottomSheet(
    sheetContent: @Composable ColumnScope.(closeSheet: () -> Unit) -> Unit,
    content: @Composable (openSheet: () -> Unit) -> Unit
) {
    val sheetScope = rememberCoroutineScope()
    val sheetState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)

    val openSheet = {
        sheetScope.launch {
            sheetState.show()
        }
    }

    val closeSheet = {
        sheetScope.launch {
            sheetState.hide()
        }
    }

    ModalBottomSheetLayout(
        sheetState = sheetState,
        sheetContent = { sheetContent(closeSheet) },
        content = { content(openSheet) }
    )
}