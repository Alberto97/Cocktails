package com.thecocktaildb.ui.cocktails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.thecocktaildb.model.Drink
import com.thecocktaildb.ui.theme.CocktailTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CocktailsFragment : Fragment() {

    private val viewModel: CocktailsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            // Dispose of the Composition when the view's LifecycleOwner
            // is destroyed
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                CocktailTheme {
                    CocktailsScreen(viewModel, ::navigate)
                }
            }
        }
    }

    private fun navigate(value: Drink) {
        val direction = CocktailsFragmentDirections.actionCocktailsToDetails(value.id)
        findNavController().navigate(direction)
    }

}