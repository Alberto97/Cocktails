package com.thecocktaildb.ui.cocktails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.thecocktaildb.data.CocktailRepository
import com.thecocktaildb.data.FilterRepository
import com.thecocktaildb.model.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.WhileSubscribed
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CocktailsViewModel @Inject constructor(
    private val filterRepository: FilterRepository,
    private val repository: CocktailRepository
) : ViewModel() {

    private val _errorMessages = MutableStateFlow(listOf<String>())
    val errorMessages = _errorMessages.asStateFlow()

    private val _uiState = MutableStateFlow(UiState.Loading)
    val uiState = _uiState.asStateFlow()

    val primaryFilterList = filterRepository.getPrimaryFilters()
    private val _primaryFilter = MutableStateFlow(primaryFilterList.first())
    val primaryFilter = _primaryFilter.asStateFlow()

    private var alcoholicFilters = listOf<AppSecondaryFilter>()
    private var categoryFilters = listOf<AppSecondaryFilter>()
    private var glassFilters = listOf<AppSecondaryFilter>()
    private var ingredientsFilters = listOf<AppSecondaryFilter>()

    val secondaryFilterList = _primaryFilter.map { filter ->
        when (filter.value) {
            CocktailFilter.Category -> loadCategoryFilters()
            CocktailFilter.Glass -> loadGlassFilters()
            CocktailFilter.Ingredient -> loadIngredientsFilters()
            CocktailFilter.Alcoholic -> loadAlcoholicFilters()
        }
    }

    private val secondaryFilterFirstItem = secondaryFilterList.map { filter ->  filter.first() }
    private val userSelectedSecondaryFilter = MutableSharedFlow<AppSecondaryFilter>()

    // The second filter has to react on its list of options changes (which means the primary
    // filter has been changed) or when the user manually changes the second filter
    val secondaryFilter = merge(secondaryFilterFirstItem, userSelectedSecondaryFilter)
        .stateIn(
            initialValue = AppSecondaryFilter("", ""),
            scope = viewModelScope,
            started = WhileSubscribed(5000)
        )

    val list = secondaryFilter.map { secondaryFilter ->
        loadCocktails(secondaryFilter.value)
    }.stateIn(
        initialValue = listOf(),
        scope = viewModelScope,
        started = WhileSubscribed(5000)
    )

    fun primaryFilterChanged(value: AppPrimaryFilter) {
        _primaryFilter.value = value
    }

    fun secondaryFilterChanged(value: AppSecondaryFilter) {
        viewModelScope.launch {
            userSelectedSecondaryFilter.emit(value)
        }
    }

    private suspend fun loadCocktails(secondaryFilter: String): List<Drink> {
        _uiState.value = UiState.Loading

        if (secondaryFilter.isEmpty()) return listOf()

        when (val response = repository.getCocktails(_primaryFilter.value.value, secondaryFilter)) {
            is Result.Success -> {
                _uiState.value = UiState.Success
                return response.data
            }
            is Result.Error -> {
                _uiState.value = UiState.Failure
                _errorMessages.emit(listOf(response.message))
            }
        }
        return listOf()
    }

    private suspend fun loadCategoryFilters(): List<AppSecondaryFilter> {
        if (categoryFilters.isEmpty()) {
            when (val response = filterRepository.getCategoryFilters()) {
                is Result.Success -> categoryFilters = response.data
                is Result.Error -> _errorMessages.emit(listOf(response.message))
            }
        }
        return categoryFilters
    }

    private suspend fun loadIngredientsFilters(): List<AppSecondaryFilter> {
        if (ingredientsFilters.isEmpty()) {
            when (val response = filterRepository.getIngredientsFilters()) {
                is Result.Success -> ingredientsFilters = response.data
                is Result.Error -> _errorMessages.emit(listOf(response.message))
            }
        }
        return ingredientsFilters
    }

    private suspend fun loadGlassFilters(): List<AppSecondaryFilter> {
        if (glassFilters.isEmpty()) {
            when (val response = filterRepository.getGlassFilters()) {
                is Result.Success -> glassFilters = response.data
                is Result.Error -> _errorMessages.emit(listOf(response.message))
            }
        }
        return glassFilters
    }

    private suspend fun loadAlcoholicFilters(): List<AppSecondaryFilter> {
        if (alcoholicFilters.isEmpty()) {
            when (val response = filterRepository.getAlcoholicFilters()) {
                is Result.Success -> alcoholicFilters = response.data
                is Result.Error -> _errorMessages.emit(listOf(response.message))
            }
        }
        return alcoholicFilters
    }

    fun dismissErrorMessage() {
        viewModelScope.launch {
            _errorMessages.emit(listOf())
        }
    }
}