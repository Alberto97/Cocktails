package com.thecocktaildb.di

import com.squareup.moshi.Moshi
import com.thecocktaildb.BuildConfig
import com.thecocktaildb.data.CocktailRepository
import com.thecocktaildb.data.CocktailRepositoryImpl
import com.thecocktaildb.data.FilterRepository
import com.thecocktaildb.data.FilterRepositoryImpl
import com.thecocktaildb.model.adapter.DateTimeAdapter
import com.thecocktaildb.network.CocktailApi
import com.thecocktaildb.tools.*
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object DispatcherModule {
    @DefaultDispatcher
    @Provides
    fun providesDefaultDispatcher(): CoroutineDispatcher = Dispatchers.Default

    @IoDispatcher
    @Provides
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @MainDispatcher
    @Provides
    fun providesMainDispatcher(): CoroutineDispatcher = Dispatchers.Main
}

@Suppress("unused")
@Module
@InstallIn(SingletonComponent::class)
abstract class DataModule {
    @Binds
    abstract fun provideCocktailRepository(value: CocktailRepositoryImpl): CocktailRepository
    @Binds
    abstract fun provideFilterRepository(value: FilterRepositoryImpl): FilterRepository
    @Binds
    abstract fun  provideUriHelper(value: UriHelperImpl): UriHelper
}

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    fun provideRetrofit(): Retrofit {
        val moshi = Moshi.Builder()
            .add(DateTimeAdapter())
            .build()

        return Retrofit.Builder()
            .baseUrl(BuildConfig.ENDPOINT_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Provides
    fun provideCocktailApi(retrofit: Retrofit): CocktailApi =
        retrofit.create(CocktailApi::class.java)
}